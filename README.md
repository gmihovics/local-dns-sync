# LocalDnsSync

LocalDnsSync is a tool for keeping local and external DNS systems in sync. It allows you to create a list of dns entries and where they resolve to in your local network. These entries are then synchronised to your external dns provider. LocalDnsSync will then periodically update the external records to point to your network's public ip address.  
